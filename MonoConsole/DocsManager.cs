﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace MonoConsole
{
	public class SentenceInFileModel
	{
		public string fileName;
		public string sentenceToFind;
		public SentenceInFileModel(string fname,string sentence){
			this.fileName = fname;
			this.sentenceToFind = sentence;
		}
	}

	public class DocsManager
	{
		//Use Volatile for multi thread
		volatile Int32 threadAlreadyDone = 0;
		Dictionary<int,int> docID_wordCount = new Dictionary<int, int> ();
		public DocsManager ()
		{
		}

		//WORD COUNT
		public void singleFileCounter(Object TaskObj){
			SentenceInFileModel taskInfo = (SentenceInFileModel)TaskObj;
			var begintime = DateTime.UtcNow;
			FileStream fs;
			try{
				fs = new FileStream (taskInfo.fileName, FileMode.Open);
			}
			catch{
				threadAlreadyDone++;
				return;
			}

			StreamReader reader = new StreamReader (fs);//M.S Here I don't know what encoding it uses

			string line;
			string[] temps;
			int docID;

			string[] sperated_line;
			while (reader.Peek() >= 0)
			{
				line = reader.ReadLine ();

				temps = line.Split ('\t');
				docID = int.Parse(temps [0]);
				sperated_line = temps [2].Split (' ', ',', '.', ':', '?', '!');
				for (int i = 0; i < sperated_line.Length; i++)
					sperated_line [i] = sperated_line [i].ToUpper ();

				foreach (string s in sperated_line) {
					if (taskInfo.sentenceToFind == s) {

						docID_wordCount [docID - 1]++;

					}
				}
			}
			reader.Close ();
			fs.Close ();

			//这里是否是线程安全的？
			threadAlreadyDone++;
			if (threadAlreadyDone == 180) {
				mulitCountResult ();
			}
			var endTime = DateTime.UtcNow;
		}

		public void mulitCountResult (){
			var sortedResult = from objDic in docID_wordCount orderby objDic.Value descending
				select objDic;
			int count = 0;
			foreach (KeyValuePair<int,int> kv in sortedResult) {
				Console.Write ("Doc" + (kv.Key+1).ToString() + " repeats " + kv.Value + " times\n");
				count++;
				if (count > 5)
					break;
			}
		}

		public void wordcount(string[] args)
		{
			if (args.Length != 3 || args [1] == "" || args [1] == null) {
				Console.WriteLine ("参数错误");
				return;
			}

			args [1] = args [1].ToUpper ();
			threadAlreadyDone = 0;

			if (args [0] != "-all") {
				int basicNum = (int.Parse (args [0].Substring (4)) - 1 ) * 100;
				for (int i = basicNum; i < basicNum + 100; i++) {
					docID_wordCount.Add (i, 0);
				}
				singleFileCounter (new SentenceInFileModel(args[0],args[1]));

				var sortedResult = from objDic in docID_wordCount orderby objDic.Value descending
				                   select objDic;
				int count = 0;
				foreach (KeyValuePair<int,int> kv in sortedResult) {
					Console.Write ("Doc" + (kv.Key+1).ToString() + " repeats " + kv.Value + " times\n");
					count++;
					if (count > 5)
						break;
				}
			} else {
				for (int i = 0; i < 18000; i++) {
					docID_wordCount.Add (i, 0);
				}
				for (int i = 1; i <= 180; i++) {
					ThreadPool.QueueUserWorkItem(new WaitCallback(singleFileCounter),new SentenceInFileModel("docs"+i.ToString(),args[1]));
				}
			}


		}
		//WORD COUNT ENDS

		public void buildindex(string[] args)
		{
		}

		public void search(string[] args)
		{
		}

		public void rank(string[] args)
		{
		}

		public void universityRank(string[] args)
		{
		}
	}
}

