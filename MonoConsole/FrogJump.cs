﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace MonoConsole
{
	class Step
	{
		public int origin;
		public int aim;
		public bool leftOrRight;
		public Step(int ori, int aimy, bool LorR){
			this.origin = ori;
			this.aim = aimy;
			this.leftOrRight = LorR;
		}
	}

	class FrogJump
	{
		const int MAX_STONES = 100;
		private int[] frogs = new int[MAX_STONES];
		private int zeroflag = 0;
		private List<Step> steps;
		static private int totalNum, leftFrog, rightFrog;

		public void entry()
		{
			Console.WriteLine ("请输入河墩的总数!");
			if (!int.TryParse (Console.ReadLine (), out totalNum)) {
				Console.WriteLine ("这不是一个数字!");
				return;
			}
			if (totalNum >= MAX_STONES) {
				Console.WriteLine ("这个数字实在太大了");
				return;
			}
			for (int i = 0; i < totalNum; i++) {
				frogs [i] = 0;
			}

			Console.WriteLine ("请输入左边的青蛙数!");
			if (!int.TryParse (Console.ReadLine (), out leftFrog)) {
				Console.WriteLine ("这不是一个数字!");
				return;
			}
			for (int i = 0; i < leftFrog; i++) {
				frogs [i] = 1;
			}

			Console.WriteLine ("请输入右边的青蛙数!");
			if (!int.TryParse (Console.ReadLine (), out rightFrog)) {
				Console.WriteLine ("这不是一个数字!");
				return;
			}
			if (leftFrog + rightFrog >= totalNum) {
				Console.WriteLine ("放不下这么多青蛙");
				return;
			}
			for (int i = 0; i < rightFrog; i++) {
				frogs [totalNum - 1 - i] = -1;
			}

			steps = new List<Step>();

			tighten ();
			startJump ();
		}

		private void tighten(){
			for (int i = 0; i < totalNum; i++) {
				frogs [i] = 0;
			}
			for (int i = 0; i < leftFrog; i++) {
				frogs [i] = 1;
			}
			for (int i = 0; i < rightFrog; i++) {
				frogs [leftFrog + 1 + i] = -1;
			}
			zeroflag = leftFrog;
		}

		private void startJump(){

			bool isDone = false;

			//Avalible after tighten


			for (int i = 0; i < rightFrog; i++) {
				if (frogs [i] != -1) {
					isDone = true;
					ThreadPool.QueueUserWorkItem(new WaitCallback(this.goSituation));
					return;
				}
			}

			if (!isDone) {
				for (int i = 0; i < leftFrog; i++) {
					if (frogs [rightFrog + i + 1] != 1) {
						ThreadPool.QueueUserWorkItem(new WaitCallback(this.goSituation));
						return;
					}
				}
			}

			Console.WriteLine ("====SUC=====");
			for (int i = 0; i < this.steps.Count; i++) {
				Console.Write (this.steps [i].origin.ToString ()+"号的青蛙");
				if (this.steps [i].leftOrRight) {
					Console.Write ("向右跳到");
				} else {
					Console.Write ("向左跳到");
				}
				Console.WriteLine (this.steps [i].aim.ToString ()+"号位置");

			}
			return;
		}

		private void goSituation(object state){

			var situ1 = new FrogJump ();
			situ1.frogs = (int[])this.frogs.Clone ();
			situ1.steps = new List<Step> (this.steps);
			situ1.zeroflag = zeroflag;
			if(zeroflag+1 < totalNum && situ1.frogs[zeroflag+1] == -1)
			{
				situ1.frogs [zeroflag + 1] = 0;
				situ1.frogs [zeroflag] = -1;
				situ1.steps.Add(new Step(zeroflag+1+1,zeroflag+1,false));
				situ1.zeroflag = zeroflag + 1;
				situ1.startJump ();

			}

			var situ2 = new FrogJump ();
			situ2.frogs = (int[])this.frogs.Clone ();
			situ2.steps = new List<Step> (this.steps);
			situ2.zeroflag = zeroflag;
			if(zeroflag+2 < totalNum && situ2.frogs[zeroflag+2] == -1)
			{
				situ2.frogs [zeroflag + 2] = 0;
				situ2.frogs [zeroflag] = -1;
				situ2.steps.Add(new Step(zeroflag+2+1,zeroflag+1,false));
				situ2.zeroflag = zeroflag + 2;
				situ2.startJump ();
			}

			var situ3 = new FrogJump ();
			situ3.frogs = (int[])this.frogs.Clone ();
			situ3.steps = new List<Step> (this.steps);
			situ3.zeroflag = zeroflag;
			if(situ3.zeroflag > 0 && situ3.frogs[zeroflag-1] == 1)
			{
				situ3.frogs [zeroflag - 1] = 0;
				situ3.frogs [zeroflag] = 1;
				situ3.steps.Add(new Step(zeroflag-1+1,zeroflag+1,true));
				situ3.zeroflag = zeroflag - 1;
				situ3.startJump ();
			}

			var situ4 = new FrogJump ();
			situ4.frogs = (int[])this.frogs.Clone ();
			situ4.steps = new List<Step> (this.steps);
			situ4.zeroflag = zeroflag;
			if(situ4.zeroflag > 1 && situ4.frogs[zeroflag-2] == 1)
			{
				situ4.frogs [zeroflag - 2] = 0;
				situ4.frogs [zeroflag] = 1;
				situ4.steps.Add(new Step(zeroflag-2+1,zeroflag+1,true));
				situ4.zeroflag = zeroflag - 2;
				situ4.startJump ();
			}
		}
	}
}

